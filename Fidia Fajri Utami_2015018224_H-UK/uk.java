public class GlobalMembers
{
    public static int Rekursif(int i, int n, int total)
    {
        int[] array = new int[50];
        if (i <= n)
        {
            System.out.print("Minuman jenis a [");
            System.out.print(i);
            System.out.print("] :");
            array[i] = Integer.parseInt(ConsoleInput.readToWhiteSpace(true));
            total += array[i];
            i += 1;
            Rekursif(i, n, total);
        }
        else
        {
            System.out.print("Jumlah seluruh minuman = ");
            System.out.print(total);
            return 0;
        }
    }
    public static void main(String[] args)
    {
        int n;
        int i = 1;
        int[] array = new int[50];
        int total;
        System.out.print("Masukan banyak jenis minuman = ");
        n = Integer.parseInt(ConsoleInput.readToWhiteSpace(true));
        Rekursif(i, n, total);

    }


}
